

__author__ = 'asus'

#import numpy
from ctypes import *
import xml.dom.minidom;

import sys

from ctypes import *;
from os import path
dirname = path.dirname(path.abspath(__file__));
#print(path.join(dirname,"liblinear.dll"));
#print(windll.kernel32);
lib =CDLL(r"C:\Users\asus\PycharmProjects\sentClassyfier\src\sentiment\RMUDLL.dll"); #<path to RMU lib>

class RMULib:

    def  __init__(self):
        self.RMU_OK=0;
        self.ERROR_CREATE ='error create';
        self.ERROR_GET_ANSWER = 'error getting answer RMU';
        self.ERROR_GET_XML = 'error getting xml RMU';
        self.handleRmu = c_void_p();
        self.error = '';
        if lib.RMUInit(pointer(self.handleRmu))!=self.RMU_OK:
            print("Error creating RMU");
            self.error = self.ERROR_CREATE;

    def findForm(self, string):
        ind =string.find(' form')
        indSpace = -1
        if ind == -1:
            return -1, -1
        for i in range(ind+3 , len(string)) :
            if string[i] == ' ' :
                indSpace = i
                break

        return ind , indSpace

    def doValidXml(self,notValidXml):
        isAtributeValue=False
        resultString = ""
        for i in range(len(notValidXml)):
            if isAtributeValue ==True and notValidXml[i]==' ' :
                resultString = resultString+r'"'
                isAtributeValue=False;
            resultString = resultString+notValidXml[i];
            if notValidXml[i] =='=' :
                isAtributeValue =True;
                resultString = resultString+r'"'
        #print(len(resultString))
        i=0
        while i < len(resultString):
            #print(i)
            #print(len(resultString))
            #print(resultString[i])
            if resultString[i] == '<' :
                startBlock=i
                for j in range(i, len(resultString)):
                    if resultString[j]=='>':
                        endBlock=j
                        break
                #print(startBlock)
                #print(endBlock)
                subString = resultString[int(startBlock):int( endBlock)]
                #print(subString)
                ind, spaceInd = self.findForm(subString)
                #print(subString[ind:spaceInd]+"|||||")
                if ind == -1:
                    i= i+1
                    continue
                subString2 = subString[spaceInd:len(subString)]
                ind2,spaceInd2 =  self.findForm(subString2)
                #print(subString2[ind2:spaceInd2]+"|||||")
                if ind2 == -1:
                    i=i+1
                    continue
                #print(resultString[0:startBlock] +"|||"+ subString[0:spaceInd]+"|||"+subString2[0:ind2]+"|||"+subString2[spaceInd2:]+resultString[endBlock:])
                resultString =  resultString[0:startBlock] + subString[0:spaceInd]+subString2[0:ind2]+subString2[spaceInd2:]+resultString[endBlock:]
            i=i+1
        return resultString;




    def getXml(self, word):
        #print(word)

        word.encode('cp1251')


        RMUAnswer = c_void_p()
        ansError=''
        answerXml = c_char_p()
        if lib.RMUGetAnswer(self.handleRmu,c_char_p(bytes(word,'cp1251')),pointer(RMUAnswer))!= self.RMU_OK:
            print('getanswer ERROR')
            ansError = self.ERROR_GET_ANSWER
        else:
            if lib.RMUGetXML(RMUAnswer,pointer(answerXml)) != self.RMU_OK:
                self.error = self.ERROR_GET_XML
                print('ERRRRRRORRRRR_______________________________________GET XML')
            else:
                s = bytes(answerXml.value).decode('cp1251')
                lib.RMUFreeAnswer(RMUAnswer)
                return s
    def getLemmaByXml(self,xmlDoc):
        xmlParsed = xml.dom.minidom.parseString(xmlDoc);
        analyzedTag =xmlParsed.getElementsByTagName('analyzed');
        if analyzedTag == []:
            return False;
        else:
            analyzedTag = analyzedTag[0]
        return analyzedTag.getAttribute('lexem');

    def getLemma(self, word):
        '''
        @return string - lemma of word
                False - if haven't lemma of word
        '''
        try :
            s =self.getXml(word)
            s1 =self.doValidXml(s)
            xmlDoc =r'<?xml version="1.0" encoding="utf-8"?>'+ s1
        except:
            print(word)
            return False
        try:
            s=self.getLemmaByXml(xmlDoc);
        except:
            print(xmlDoc);
            return False
        return s;
    def free(self):
        lib.RMUFree(self.handleRmu)
    def __del__(self):
        if self.error != self.ERROR_CREATE:
            pass
            #if lib.RMUFree(self.handleRmu)!= self.RMU_OK:
            #print('Error deleting handle');


mypointer = c_void_p();
answerPointer= c_void_p();
text = c_char_p();
lenz=lib.RMUInit(pointer(mypointer));
print(lenz);
line = "экранизирован";
line.encode('cp1251','ignore');
print(line);
lenz =lib.RMUGetAnswer(mypointer,c_char_p(bytes(line,'cp1251')),pointer(answerPointer));
print(lenz)
lenz=lib.RMUGetXML(answerPointer, pointer(text));
text1 =bytes(text.value).decode('cp1251');
lib.RMUFree(mypointer);
#print(text1);
#print(lenz);



rmuLib = RMULib();
#for i in range(2000000):
#    print(i)
#    rmuLib.getLemma("мозга");



