__author__ = 'asus'

import sqlite3
from pickle import dump
from pickle import load
from src.sentiment.ir import tokenize
import src.sentiment.lemma as lemma
def dumpFromBase(filename):
    con = sqlite3.connect('test2.db')
    con.row_factory = sqlite3.Row
    cur = con.cursor()

    docs = []
    cur.execute('select class, text from docs')
    for row in cur.fetchall():
        docs.append((row['class'], row['text']))

    con.close()

    file = open(filename , 'ab')
    dump(docs,file)

    file.close()
    for doc in docs :
        print(doc)
def loadFromPickle(filename):

    file = open(filename , 'rb')
    docs = load(file)
    file.close()

    #for doc in docs :
    #    print(doc)
    return docs
def dumpFromPickle(docs_,filename):
    file = open(filename , 'ab')
    dump(docs_,file)

    file.close()

def lemmaFromAllDocs(docs):
    lemmadocs = []
    i=0
    for doc in docs :

        lemmaWordList = []
        wordlist = tokenize(doc[1])
        for word in wordlist:
            try:
                s= lemma.rmuLib.getLemma(word)
                if s :
                    lemmaWordList.append(s+' ')
                else :
                    lemmaWordList.append(word+' ')
            except : continue
        lemmadocs.append((doc[0], ''.join(lemmaWordList)))
        i= i+1
        if i % 1000 == 0 :
            print(i)
            print(doc)
            print(''.join(lemmaWordList))
    return lemmadocs


#dumpFromBase('docsPickle1.txt')
#docs = loadFromPickle('docsPickle1.txt')
#lemmaDocs = lemmaFromAllDocs(docs[0:5000])
#dumpFromPickle(lemmaDocs,'lemmaDocsPickle.txt')
#lemmaDocs = [('pos','sdfsdfs'),('pos','ferrere')]# = loadFromPickle('lemmaDocsPickle.txt')
lemmaDocs = loadFromPickle('lemmaDocsPickleStep2.txt')
#lemmaDocs.extend(lemmaFromAllDocs(docs[10000:]))
#dumpFromPickle(lemmaDocs, 'lemmaDocsPickleStep2.txt' )
#print(len(lemmaDocs))

#for doc in lemmaDocs:
#    print(doc)
##################################################
con = sqlite3.connect('lemmaDB3.db')
con.row_factory = sqlite3.Row
cur = con.cursor()
cur.execute('''
	create table if not exists docs(
		id integer primary key autoincrement,
		text text,
		class text
	)	''')

cur.executemany('insert into docs (class, text) values (?, ?)', lemmaDocs)
con.commit()
con.close()
