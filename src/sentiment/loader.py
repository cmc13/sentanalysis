import xml.dom.minidom;
import xml.dom
import sqlite3;
document ="""<?xml version="1.0" encoding="utf-8"?><table>
<columns>
<column number="0">
<name>score</name>
</column>
<column number="1">
<name>content_id</name>
</column>
<column number="2">
<name>element_id</name>
</column>
<column number="3">
<name>user_id</name>
</column>
<column number="4">
<name>text</name>
</column>
</columns>
<rows>
<row rowNumber="0">
<value columnNumber="0">10</value>
<value columnNumber="1">3</value>
<value columnNumber="2">196076</value>
<value columnNumber="3">23499</value>
<value columnNumber="4">Замечательный фильм, очень
рекомендую.</value>
</row>
 </rows>
 </table>"""

def parse(filename):
    xmlDoc = xml.dom.minidom.parse(filename);
    rows = (xmlDoc.getElementsByTagName("rows"))[0];
    rows = rows.getElementsByTagName("row");
    docs=[];
    text="undefined"
    sent = "undefined"
    for row in rows:
        values = row.getElementsByTagName("value")
        for value in values:
            if value.childNodes != [] and len(value.childNodes) < 2 :#TODO kyky epta!!
                if value.getAttribute("columnNumber")=="0":
                    grade = [val.data for val in  value.childNodes][0];
                    if(int(grade)>5):
                        sent = "pos";
                    else:
                        sent = "neg"
                if value.getAttribute("columnNumber")=="4" :
                    if sent == "undefined":
                        print("error");
                    if len(value.childNodes) < 2:
                        text = [val.data for val in  value.childNodes][0];
                        docs.append((sent ,text ))
                    sent="undefined"
    return docs;

def clear(text) :
    flag = False
    res = []
    for i in range(len(text)) :
        if flag and text[i] == 'n' :
            flag =False
            res.append(' ')
            continue
        else :
            flag = False
        if text[i] == '\\':
            flag = True
            continue
        if text[i] == '\n':
            res.append(' ')
            continue
        res.append(text[i])
    return ''.join(res)
con = sqlite3.connect('test2.db')
cur = con.cursor()


cur.execute('''
	create table if not exists docs(
		id integer primary key autoincrement,
		text text,
		class text
	)
	''')
filename = 'films1.xml';

texts = parse(filename);
clearTexts = []
for text in texts :
    clearText = clear(text[1])
    clearTexts.append((text[0],clearText))
print(len(texts))
cur.executemany('insert into docs (class, text) values (?, ?)', clearTexts)

con.commit()
con.close()
