#!/usr/bin/env python3

import sqlite3
from Tools.Scripts.ndiff import fopen
import src.sentiment.ir as ir

from src.sentiment.ml.svm import SVM
from src.sentiment.ml.svm2 import SVM2
from src.sentiment.ml.nb import NaiveBayes
import src.sentiment.ml as ml

fileNameToSave = 'classOLD.txt'#'classifier.txt';

con = sqlite3.connect('res/test2.db');#'lemmaDB3.db');
con.row_factory = sqlite3.Row
cur = con.cursor()



docs = []
cur.execute('select class, text from docs')
for row in cur.fetchall():
    docs.append((row['class'], row['text']))

con.close()

docs_even = []
N = int(len(docs) / 2)
for i in range(N):
    docs_even.append(docs[i])
    docs_even.append(docs[N + i])

def    test(classifier, features, weight, kerneltype,koef1=0,koef2=0,koef3=0):
    p = []
    f = open(fileNameToSave, 'a')
    f.write('>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>')
    f.close()
    for fold in range(1, 7):
        train_docs, test_docs = ml.folds(docs_even, 6, fold)

        index = ir.SentimentIndex(weight, features)
        index.get_class = lambda x: x[0]
        index.get_text = lambda x: x[1]
        index.build(train_docs)

        train_x = []
        train_y = []
        for doc in train_docs:
            train_x.append(index.weight(index.features(doc)))
            train_y.append(doc[0])
            #for item in index.weight(index.features(doc)).items():
            #    print("doc 0 train_x  "+item[0] +":"+str(item[1])  )
        test_x = []
        test_y = []
        for doc in test_docs:
            test_x.append(index.weight(index.features(doc)))
            test_y.append(doc[0])


        #f= open('wordlist');
        #lines = f.readlines();
        #words = []
        #for line in lines:
        #    words.append(ir.tokenize(line)[0]);

        cl = classifier()
        cl.train(train_x, train_y, kerneltype,koef1,koef2,koef3)
        #cl.setAllId(words)
        labels = cl.predict(test_x)

        f =open(fileNameToSave,'a')
        f.write('\n ------------------------------------------------------\n')
        f.close()

        mic, mac,results,errors = cl.evaluate(test_y, labels)
        print('mic:' + str(mic))
        print('mac:' + str(mac))
        print('results:' + str(results))
        print('errors:' + str(errors))

        f =open(fileNameToSave, 'a')
        f.write('mic:' + str(mic)+'\n')
        f.write('mac:' + str(mac)+'\n')
        f.write('results:' + str(results)+'\n')
        f.write('errors:' + str(errors)+'\n')
        f.close()

        p.append(mic)
    f =open(fileNameToSave,'a')
    f.write('\n'+'{0} {1} {2} {3}: {3:.1f}% '.format(classifier, features, weight, ir.avg(p) * 100) + '__'+kerneltype+ ' koef1 =' + str(koef1) + ' koef2 =' + str(koef2) + ' koef3 =' + str(koef3) +   '\n\n\n')
    f.close();
    print('{0} {1} {2} {3}: {3:.1f}%'.format(classifier, features, weight, ir.avg(p) * 100) + '__'+kerneltype+ ' koef1 =' + str(koef1) + ' koef2 =' + str(koef2) + ' koef3 =' + str(koef3))

test(NaiveBayes, 'unigram', 'bin','linear')
#test(SVM2, 'unigram', 'delta','polynomial',2,0,1)
#test(SVM2, 'bigram', 'delta','polynomial',2,0,1)
#test(SVM2, 'bogram', 'delta','polynomial',2,0,1)
#test(SVM2, 'unigram', 'bin','polynomial',2,0,1)
#test(SVM2, 'bigram', 'bin','polynomial',2,0,1)

#test(SVM2, 'unigram', 'bin','sigmoid',1,0)
#test(SVM2, 'bigram', 'bin','sigmoid',1,0)
#test(SVM2, 'bogram', 'bin','sigmoid',1,0)

#test(SVM2, 'unigram', 'delta','sigmoid',1,0)
#test(SVM2, 'bigram', 'delta','sigmoid',1,0)
#test(SVM2, 'bogram', 'delta','sigmoid',1,0)
#
#-----------------------------------------------------
#test(NaiveBayes, 'bigram', 'bin','linear')
#test(NaiveBayes, 'bogram', 'bin','linear')
#test(SVM, 'unigram', 'bin','linear')
#test(SVM, 'bigram', 'bin','linear')
#test(SVM, 'bogram', 'bin','linear')
#test(SVM, 'unigram', 'delta','linear')
#test(SVM, 'bigram', 'delta','linear')
#test(SVM, 'bogram', 'delta','linear')

#test(SVM2, 'bogram', 'bin','polynomial',2,0,1)


#test(SVM2, 'unigram', 'delta','radial_basis')
#test(SVM2, 'bigram', 'delta','polynomial')
#test(SVM2, 'bogram', 'delta','polynomial')

# polynomial -d -r -g
# sigmoid -g -r
'''
           -s svm_type : set type of SVM (default 0)
           0 -- C-SVC		(multi-class classification)
           1 -- nu-SVC		(multi-class classification)
           2 -- one-class SVM
           3 -- epsilon-SVR	(regression)
           4 -- nu-SVR		(regression)
       -t kernel_type : set type of kernel function (default 2)
           0 -- linear: u'*v
           1 -- polynomial: (gamma*u'*v + coef0)^degree
           2 -- radial basis function: exp(-gamma*|u-v|^2)
           3 -- sigmoid: tanh(gamma*u'*v + coef0)
           4 -- precomputed kernel (kernel values in training_set_file)
       -d degree : set degree in kernel function (default 3)
       -g gamma : set gamma in kernel function (default 1/num_features)
       -r coef0 : set coef0 in kernel function (default 0)
       -c cost : set the parameter C of C-SVC, epsilon-SVR, and nu-SVR (default 1)
       -n nu : set the parameter nu of nu-SVC, one-class SVM, and nu-SVR (default 0.5)
       -p epsilon : set the epsilon in loss function of epsilon-SVR (default 0.1)
       -m cachesize : set cache memory size in MB (default 100)
       -e epsilon : set tolerance of termination criterion (default 0.001)
       -h shrinking : whether to use the shrinking heuristics, 0 or 1 (default 1)
       -b probability_estimates : whether to train a SVC or SVR model for probability estimates, 0 or 1 (default 0)
       -wi weight : set the parameter C of class i to weight*C, for C-SVC (default 1)
       -v n: n-fold cross validation mode
       -q : quiet mode (no outputs)
   '''