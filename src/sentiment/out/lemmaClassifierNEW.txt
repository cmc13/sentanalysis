>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
 ------------------------------------------------------
mic:0.7778723404255319
mac:0.5844284933225057
results:defaultdict(<function Classifier.evaluate.<locals>.<lambda> at 0x0DE82CD8>, {'neg': {'miss': 433, 'hit': 121, 'acc': 0.2184115523465704}, 'pos': {'miss': 89, 'hit': 1707, 'acc': 0.950445434298441}})
errors:defaultdict(<function Classifier.evaluate.<locals>.<lambda> at 0x0E5DF078>, {'neg': defaultdict(<class 'int'>, {None: 17, 'neg': 121, 'pos': 416}), 'pos': defaultdict(<class 'int'>, {None: 32, 'neg': 57, 'pos': 1707})})

 ------------------------------------------------------
mic:0.7387234042553191
mac:0.5798396962128549
results:defaultdict(<function Classifier.evaluate.<locals>.<lambda> at 0x088A4F18>, {'neg': {'miss': 497, 'hit': 147, 'acc': 0.22826086956521738}, 'pos': {'miss': 117, 'hit': 1589, 'acc': 0.9314185228604924}})
errors:defaultdict(<function Classifier.evaluate.<locals>.<lambda> at 0x0CBD6978>, {'neg': defaultdict(<class 'int'>, {None: 9, 'neg': 147, 'pos': 488}), 'pos': defaultdict(<class 'int'>, {None: 20, 'neg': 97, 'pos': 1589})})

 ------------------------------------------------------
mic:0.731063829787234
mac:0.5670790418902545
results:defaultdict(<function Classifier.evaluate.<locals>.<lambda> at 0x0969F4F8>, {'pos': {'miss': 107, 'hit': 1589, 'acc': 0.9369103773584906}, 'neg': {'miss': 525, 'hit': 129, 'acc': 0.19724770642201836}})
errors:defaultdict(<function Classifier.evaluate.<locals>.<lambda> at 0x0969F6A8>, {'pos': defaultdict(<class 'int'>, {None: 33, 'neg': 74, 'pos': 1589}), 'neg': defaultdict(<class 'int'>, {None: 29, 'neg': 129, 'pos': 496})})

 ------------------------------------------------------
mic:0.7604255319148936
mac:0.5696224100661841
results:defaultdict(<function Classifier.evaluate.<locals>.<lambda> at 0x10948108>, {'neg': {'miss': 460, 'hit': 113, 'acc': 0.19720767888307156}, 'pos': {'miss': 103, 'hit': 1674, 'acc': 0.9420371412492966}})
errors:defaultdict(<function Classifier.evaluate.<locals>.<lambda> at 0x0A8C7618>, {'neg': defaultdict(<class 'int'>, {None: 13, 'pos': 447, 'neg': 113}), 'pos': defaultdict(<class 'int'>, {None: 45, 'neg': 58, 'pos': 1674})})

 ------------------------------------------------------
mic:0.7531914893617021
mac:0.5621190476190476
results:defaultdict(<function Classifier.evaluate.<locals>.<lambda> at 0x0D931348>, {'pos': {'miss': 83, 'hit': 1667, 'acc': 0.9525714285714286}, 'neg': {'miss': 497, 'hit': 103, 'acc': 0.17166666666666666}})
errors:defaultdict(<function Classifier.evaluate.<locals>.<lambda> at 0x0BFCDB28>, {'pos': defaultdict(<class 'int'>, {None: 28, 'neg': 55, 'pos': 1667}), 'neg': defaultdict(<class 'int'>, {None: 11, 'neg': 103, 'pos': 486})})

 ------------------------------------------------------
mic:0.7753191489361703
mac:0.5630348563958198
results:defaultdict(<function Classifier.evaluate.<locals>.<lambda> at 0x0DF61348>, {'neg': {'miss': 467, 'hit': 89, 'acc': 0.16007194244604317}, 'pos': {'miss': 61, 'hit': 1733, 'acc': 0.9659977703455964}})
errors:defaultdict(<function Classifier.evaluate.<locals>.<lambda> at 0x0AB36DF8>, {'neg': defaultdict(<class 'int'>, {None: 7, 'neg': 89, 'pos': 460}), 'pos': defaultdict(<class 'int'>, {None: 28, 'neg': 33, 'pos': 1733})})

<class 'yatk.ml.nb.NaiveBayes'> unigram bin 75.60992907801419: 75.6% __linear koef1 =0 koef2 =0 koef3 =0


>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
 ------------------------------------------------------
mic:0.731063829787234
mac:0.5325844435689417
results:defaultdict(<function Classifier.evaluate.<locals>.<lambda> at 0x10E3B4B0>, {'neg': {'miss': 467, 'hit': 87, 'acc': 0.15703971119133575}, 'pos': {'miss': 165, 'hit': 1631, 'acc': 0.9081291759465479}})
errors:defaultdict(<function Classifier.evaluate.<locals>.<lambda> at 0x1169A810>, {'neg': defaultdict(<class 'int'>, {None: 32, 'neg': 87, 'pos': 435}), 'pos': defaultdict(<class 'int'>, {None: 87, 'neg': 78, 'pos': 1631})})

 ------------------------------------------------------
mic:0.6957446808510638
mac:0.5072233185032002
results:defaultdict(<function Classifier.evaluate.<locals>.<lambda> at 0x02A8A738>, {'neg': {'miss': 586, 'hit': 58, 'acc': 0.09006211180124224}, 'pos': {'miss': 129, 'hit': 1577, 'acc': 0.9243845252051582}})
errors:defaultdict(<function Classifier.evaluate.<locals>.<lambda> at 0x10636C48>, {'neg': defaultdict(<class 'int'>, {None: 18, 'neg': 58, 'pos': 568}), 'pos': defaultdict(<class 'int'>, {None: 65, 'neg': 64, 'pos': 1577})})

 ------------------------------------------------------
mic:0.6974468085106383
mac:0.5254700752985979
results:defaultdict(<function Classifier.evaluate.<locals>.<lambda> at 0x0E56AF18>, {'pos': {'miss': 147, 'hit': 1549, 'acc': 0.9133254716981132}, 'neg': {'miss': 564, 'hit': 90, 'acc': 0.13761467889908258}})
errors:defaultdict(<function Classifier.evaluate.<locals>.<lambda> at 0x0E782738>, {'pos': defaultdict(<class 'int'>, {None: 71, 'neg': 76, 'pos': 1549}), 'neg': defaultdict(<class 'int'>, {None: 42, 'neg': 90, 'pos': 522})})

 ------------------------------------------------------
mic:0.7144680851063829
mac:0.5226797522345346
results:defaultdict(<function Classifier.evaluate.<locals>.<lambda> at 0x0BC34F60>, {'neg': {'miss': 488, 'hit': 85, 'acc': 0.14834205933682373}, 'pos': {'miss': 183, 'hit': 1594, 'acc': 0.8970174451322454}})
errors:defaultdict(<function Classifier.evaluate.<locals>.<lambda> at 0x153A0ED0>, {'neg': defaultdict(<class 'int'>, {None: 31, 'pos': 457, 'neg': 85}), 'pos': defaultdict(<class 'int'>, {None: 98, 'neg': 85, 'pos': 1594})})

 ------------------------------------------------------
mic:0.725531914893617
mac:0.5413571428571429
results:defaultdict(<function Classifier.evaluate.<locals>.<lambda> at 0x0BAD2FA8>, {'pos': {'miss': 144, 'hit': 1606, 'acc': 0.9177142857142857}, 'neg': {'miss': 501, 'hit': 99, 'acc': 0.165}})
errors:defaultdict(<function Classifier.evaluate.<locals>.<lambda> at 0x16911F18>, {'pos': defaultdict(<class 'int'>, {None: 65, 'neg': 79, 'pos': 1606}), 'neg': defaultdict(<class 'int'>, {None: 34, 'neg': 99, 'pos': 467})})

 ------------------------------------------------------
mic:0.7340425531914894
mac:0.5229682474755981
results:defaultdict(<function Classifier.evaluate.<locals>.<lambda> at 0x0CD249C0>, {'neg': {'miss': 488, 'hit': 68, 'acc': 0.1223021582733813}, 'pos': {'miss': 137, 'hit': 1657, 'acc': 0.9236343366778149}})
errors:defaultdict(<function Classifier.evaluate.<locals>.<lambda> at 0x09FCBB70>, {'neg': defaultdict(<class 'int'>, {None: 20, 'neg': 68, 'pos': 468}), 'pos': defaultdict(<class 'int'>, {None: 57, 'neg': 80, 'pos': 1657})})

<class 'yatk.ml.nb.NaiveBayes'> bigram bin 71.63829787234043: 71.6% __linear koef1 =0 koef2 =0 koef3 =0


>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
 ------------------------------------------------------
mic:0.762127659574468
mac:0.5254456353066984
results:defaultdict(<function Classifier.evaluate.<locals>.<lambda> at 0x16AA7F60>, {'neg': {'miss': 511, 'hit': 43, 'acc': 0.0776173285198556}, 'pos': {'miss': 48, 'hit': 1748, 'acc': 0.9732739420935412}})
errors:defaultdict(<function Classifier.evaluate.<locals>.<lambda> at 0x12AF3300>, {'neg': defaultdict(<class 'int'>, {None: 17, 'neg': 43, 'pos': 494}), 'pos': defaultdict(<class 'int'>, {None: 32, 'neg': 16, 'pos': 1748})})

 ------------------------------------------------------
mic:0.7225531914893617
mac:0.5082882482724472
results:defaultdict(<function Classifier.evaluate.<locals>.<lambda> at 0x10C37030>, {'neg': {'miss': 622, 'hit': 22, 'acc': 0.034161490683229816}, 'pos': {'miss': 30, 'hit': 1676, 'acc': 0.9824150058616647}})
errors:defaultdict(<function Classifier.evaluate.<locals>.<lambda> at 0x10C37C48>, {'neg': defaultdict(<class 'int'>, {None: 9, 'neg': 22, 'pos': 613}), 'pos': defaultdict(<class 'int'>, {None: 20, 'neg': 10, 'pos': 1676})})

 ------------------------------------------------------
mic:0.7170212765957447
mac:0.5117879450118286
results:defaultdict(<function Classifier.evaluate.<locals>.<lambda> at 0x0907EAE0>, {'pos': {'miss': 43, 'hit': 1653, 'acc': 0.9746462264150944}, 'neg': {'miss': 622, 'hit': 32, 'acc': 0.04892966360856269}})
errors:defaultdict(<function Classifier.evaluate.<locals>.<lambda> at 0x0BD6DB70>, {'pos': defaultdict(<class 'int'>, {None: 33, 'neg': 10, 'pos': 1653}), 'neg': defaultdict(<class 'int'>, {None: 29, 'neg': 32, 'pos': 593})})

 ------------------------------------------------------
mic:0.7459574468085106
mac:0.5174873627630937
results:defaultdict(<function Classifier.evaluate.<locals>.<lambda> at 0x0BFA4F60>, {'neg': {'miss': 532, 'hit': 41, 'acc': 0.07155322862129145}, 'pos': {'miss': 65, 'hit': 1712, 'acc': 0.9634214969048959}})
errors:defaultdict(<function Classifier.evaluate.<locals>.<lambda> at 0x0BFA4738>, {'neg': defaultdict(<class 'int'>, {None: 13, 'neg': 41, 'pos': 519}), 'pos': defaultdict(<class 'int'>, {None: 45, 'neg': 20, 'pos': 1712})})

 ------------------------------------------------------
mic:0.7451063829787234
mac:0.5238333333333334
results:defaultdict(<function Classifier.evaluate.<locals>.<lambda> at 0x099E9ED0>, {'pos': {'miss': 42, 'hit': 1708, 'acc': 0.976}, 'neg': {'miss': 557, 'hit': 43, 'acc': 0.07166666666666667}})
errors:defaultdict(<function Classifier.evaluate.<locals>.<lambda> at 0x108CD6A8>, {'pos': defaultdict(<class 'int'>, {None: 28, 'neg': 14, 'pos': 1708}), 'neg': defaultdict(<class 'int'>, {None: 11, 'neg': 43, 'pos': 546})})

 ------------------------------------------------------
mic:0.7612765957446809
mac:0.5166031054754858
results:defaultdict(<function Classifier.evaluate.<locals>.<lambda> at 0x108CD660>, {'neg': {'miss': 527, 'hit': 29, 'acc': 0.052158273381294966}, 'pos': {'miss': 34, 'hit': 1760, 'acc': 0.9810479375696767}})
errors:defaultdict(<function Classifier.evaluate.<locals>.<lambda> at 0x108CD6F0>, {'neg': defaultdict(<class 'int'>, {None: 7, 'neg': 29, 'pos': 520}), 'pos': defaultdict(<class 'int'>, {None: 28, 'neg': 6, 'pos': 1760})})

<class 'yatk.ml.nb.NaiveBayes'> bogram bin 74.23404255319149: 74.2% __linear koef1 =0 koef2 =0 koef3 =0


>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
 ------------------------------------------------------
mic:0.7770212765957447
mac:0.6512778094924139
results:defaultdict(<function Classifier.evaluate.<locals>.<lambda> at 0x108CD810>, {'neg': {'miss': 325, 'hit': 229, 'acc': 0.41335740072202165}, 'pos': {'miss': 199, 'hit': 1597, 'acc': 0.8891982182628062}})
errors:defaultdict(<function Classifier.evaluate.<locals>.<lambda> at 0x108CD8A0>, {'neg': defaultdict(<class 'int'>, {'neg': 229, 'pos': 325}), 'pos': defaultdict(<class 'int'>, {'neg': 199, 'pos': 1597})})

 ------------------------------------------------------
mic:0.7251063829787234
mac:0.6221756606205355
results:defaultdict(<function Classifier.evaluate.<locals>.<lambda> at 0x16ABA5D0>, {'neg': {'miss': 390, 'hit': 254, 'acc': 0.3944099378881988}, 'pos': {'miss': 256, 'hit': 1450, 'acc': 0.8499413833528722}})
errors:defaultdict(<function Classifier.evaluate.<locals>.<lambda> at 0x16ABA270>, {'neg': defaultdict(<class 'int'>, {'neg': 254, 'pos': 390}), 'pos': defaultdict(<class 'int'>, {'pos': 1450, 'neg': 256})})

 ------------------------------------------------------
mic:0.7523404255319149
mac:0.6522768088973516
results:defaultdict(<function Classifier.evaluate.<locals>.<lambda> at 0x088A4F18>, {'pos': {'miss': 207, 'hit': 1489, 'acc': 0.8779481132075472}, 'neg': {'miss': 375, 'hit': 279, 'acc': 0.42660550458715596}})
errors:defaultdict(<function Classifier.evaluate.<locals>.<lambda> at 0x1B92BF18>, {'pos': defaultdict(<class 'int'>, {'neg': 207, 'pos': 1489}), 'neg': defaultdict(<class 'int'>, {'pos': 375, 'neg': 279})})

 ------------------------------------------------------
mic:0.7693617021276595
mac:0.6452960604819582
results:defaultdict(<function Classifier.evaluate.<locals>.<lambda> at 0x18277030>, {'neg': {'miss': 342, 'hit': 231, 'acc': 0.4031413612565445}, 'pos': {'miss': 200, 'hit': 1577, 'acc': 0.887450759707372}})
errors:defaultdict(<function Classifier.evaluate.<locals>.<lambda> at 0x13B3ECD8>, {'neg': defaultdict(<class 'int'>, {'pos': 342, 'neg': 231}), 'pos': defaultdict(<class 'int'>, {'neg': 200, 'pos': 1577})})

 ------------------------------------------------------
mic:0.7757446808510639
mac:0.6506428571428571
results:defaultdict(<function Classifier.evaluate.<locals>.<lambda> at 0x0DAD9780>, {'pos': {'miss': 164, 'hit': 1586, 'acc': 0.9062857142857143}, 'neg': {'miss': 363, 'hit': 237, 'acc': 0.395}})
errors:defaultdict(<function Classifier.evaluate.<locals>.<lambda> at 0x0B377B70>, {'pos': defaultdict(<class 'int'>, {'neg': 164, 'pos': 1586}), 'neg': defaultdict(<class 'int'>, {'neg': 237, 'pos': 363})})

 ------------------------------------------------------
mic:0.7851063829787234
mac:0.6488785560180619
results:defaultdict(<function Classifier.evaluate.<locals>.<lambda> at 0x0B3FD078>, {'neg': {'miss': 339, 'hit': 217, 'acc': 0.3902877697841727}, 'pos': {'miss': 166, 'hit': 1628, 'acc': 0.907469342251951}})
errors:defaultdict(<function Classifier.evaluate.<locals>.<lambda> at 0x1B0A2A98>, {'neg': defaultdict(<class 'int'>, {'neg': 217, 'pos': 339}), 'pos': defaultdict(<class 'int'>, {'neg': 166, 'pos': 1628})})

<class 'yatk.ml.svm.SVM'> unigram bin 76.4113475177305: 76.4% __linear koef1 =0 koef2 =0 koef3 =0


>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
 ------------------------------------------------------
mic:0.7408510638297873
mac:0.6138832383234303
results:defaultdict(<function Classifier.evaluate.<locals>.<lambda> at 0x12E49CD8>, {'neg': {'miss': 347, 'hit': 207, 'acc': 0.37364620938628157}, 'pos': {'miss': 262, 'hit': 1534, 'acc': 0.8541202672605791}})
errors:defaultdict(<function Classifier.evaluate.<locals>.<lambda> at 0x0B377078>, {'neg': defaultdict(<class 'int'>, {'pos': 347, 'neg': 207}), 'pos': defaultdict(<class 'int'>, {'neg': 262, 'pos': 1534})})

 ------------------------------------------------------
mic:0.694468085106383
mac:0.5590253253041877
results:defaultdict(<function Classifier.evaluate.<locals>.<lambda> at 0x089573D8>, {'neg': {'miss': 477, 'hit': 167, 'acc': 0.2593167701863354}, 'pos': {'miss': 241, 'hit': 1465, 'acc': 0.8587338804220399}})
errors:defaultdict(<function Classifier.evaluate.<locals>.<lambda> at 0x0CAAB078>, {'neg': defaultdict(<class 'int'>, {'neg': 167, 'pos': 477}), 'pos': defaultdict(<class 'int'>, {'neg': 241, 'pos': 1465})})

 ------------------------------------------------------
mic:0.7029787234042553
mac:0.5922443886677052
results:defaultdict(<function Classifier.evaluate.<locals>.<lambda> at 0x0C7CFD68>, {'pos': {'miss': 268, 'hit': 1428, 'acc': 0.8419811320754716}, 'neg': {'miss': 430, 'hit': 224, 'acc': 0.3425076452599388}})
errors:defaultdict(<function Classifier.evaluate.<locals>.<lambda> at 0x0DF7C030>, {'pos': defaultdict(<class 'int'>, {'neg': 268, 'pos': 1428}), 'neg': defaultdict(<class 'int'>, {'neg': 224, 'pos': 430})})

 ------------------------------------------------------
mic:0.7165957446808511
mac:0.6027198417632321
results:defaultdict(<function Classifier.evaluate.<locals>.<lambda> at 0x0DF9C198>, {'neg': {'miss': 355, 'hit': 218, 'acc': 0.38045375218150085}, 'pos': {'miss': 311, 'hit': 1466, 'acc': 0.8249859313449635}})
errors:defaultdict(<function Classifier.evaluate.<locals>.<lambda> at 0x02A8A738>, {'neg': defaultdict(<class 'int'>, {'pos': 355, 'neg': 218}), 'pos': defaultdict(<class 'int'>, {'neg': 311, 'pos': 1466})})

 ------------------------------------------------------
mic:0.7361702127659574
mac:0.6103809523809524
results:defaultdict(<function Classifier.evaluate.<locals>.<lambda> at 0x1A7D6FA8>, {'pos': {'miss': 232, 'hit': 1518, 'acc': 0.8674285714285714}, 'neg': {'miss': 388, 'hit': 212, 'acc': 0.35333333333333333}})
errors:defaultdict(<function Classifier.evaluate.<locals>.<lambda> at 0x12624660>, {'pos': defaultdict(<class 'int'>, {'neg': 232, 'pos': 1518}), 'neg': defaultdict(<class 'int'>, {'neg': 212, 'pos': 388})})

 ------------------------------------------------------
mic:0.7421276595744681
mac:0.5971473657194646
results:defaultdict(<function Classifier.evaluate.<locals>.<lambda> at 0x12B13978>, {'neg': {'miss': 377, 'hit': 179, 'acc': 0.32194244604316546}, 'pos': {'miss': 229, 'hit': 1565, 'acc': 0.8723522853957637}})
errors:defaultdict(<function Classifier.evaluate.<locals>.<lambda> at 0x0DAFF270>, {'neg': defaultdict(<class 'int'>, {'neg': 179, 'pos': 377}), 'pos': defaultdict(<class 'int'>, {'neg': 229, 'pos': 1565})})

<class 'yatk.ml.svm.SVM'> bigram bin 72.21985815602837: 72.2% __linear koef1 =0 koef2 =0 koef3 =0


>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
 ------------------------------------------------------
mic:0.7859574468085107
mac:0.6558758733808785
results:defaultdict(<function Classifier.evaluate.<locals>.<lambda> at 0x192C4ED0>, {'neg': {'miss': 327, 'hit': 227, 'acc': 0.40974729241877256}, 'pos': {'miss': 176, 'hit': 1620, 'acc': 0.9020044543429844}})
errors:defaultdict(<function Classifier.evaluate.<locals>.<lambda> at 0x02A8A738>, {'neg': defaultdict(<class 'int'>, {'neg': 227, 'pos': 327}), 'pos': defaultdict(<class 'int'>, {'neg': 176, 'pos': 1620})})

 ------------------------------------------------------
mic:0.72
mac:0.6143088332738672
results:defaultdict(<function Classifier.evaluate.<locals>.<lambda> at 0x089573D8>, {'neg': {'miss': 399, 'hit': 245, 'acc': 0.3804347826086957}, 'pos': {'miss': 259, 'hit': 1447, 'acc': 0.8481828839390387}})
errors:defaultdict(<function Classifier.evaluate.<locals>.<lambda> at 0x0A8D01E0>, {'neg': defaultdict(<class 'int'>, {'neg': 245, 'pos': 399}), 'pos': defaultdict(<class 'int'>, {'pos': 1447, 'neg': 259})})

 ------------------------------------------------------
mic:0.7459574468085106
mac:0.636581486930933
results:defaultdict(<function Classifier.evaluate.<locals>.<lambda> at 0x10E08C90>, {'pos': {'miss': 198, 'hit': 1498, 'acc': 0.8832547169811321}, 'neg': {'miss': 399, 'hit': 255, 'acc': 0.38990825688073394}})
errors:defaultdict(<function Classifier.evaluate.<locals>.<lambda> at 0x09678540>, {'pos': defaultdict(<class 'int'>, {'neg': 198, 'pos': 1498}), 'neg': defaultdict(<class 'int'>, {'neg': 255, 'pos': 399})})

 ------------------------------------------------------
mic:0.7740425531914894
mac:0.6472087100933884
results:defaultdict(<function Classifier.evaluate.<locals>.<lambda> at 0x12BD56F0>, {'neg': {'miss': 344, 'hit': 229, 'acc': 0.39965095986038396}, 'pos': {'miss': 187, 'hit': 1590, 'acc': 0.8947664603263928}})
errors:defaultdict(<function Classifier.evaluate.<locals>.<lambda> at 0x02A8A738>, {'neg': defaultdict(<class 'int'>, {'pos': 344, 'neg': 229}), 'pos': defaultdict(<class 'int'>, {'neg': 187, 'pos': 1590})})

 ------------------------------------------------------
mic:0.7731914893617021
mac:0.6412619047619047
results:defaultdict(<function Classifier.evaluate.<locals>.<lambda> at 0x089573D8>, {'pos': {'miss': 156, 'hit': 1594, 'acc': 0.9108571428571428}, 'neg': {'miss': 377, 'hit': 223, 'acc': 0.37166666666666665}})
errors:defaultdict(<function Classifier.evaluate.<locals>.<lambda> at 0x12947540>, {'pos': defaultdict(<class 'int'>, {'neg': 156, 'pos': 1594}), 'neg': defaultdict(<class 'int'>, {'neg': 223, 'pos': 377})})

 ------------------------------------------------------
mic:0.7897872340425532
mac:0.6494620357225925
results:defaultdict(<function Classifier.evaluate.<locals>.<lambda> at 0x0B8A0D68>, {'neg': {'miss': 343, 'hit': 213, 'acc': 0.3830935251798561}, 'pos': {'miss': 151, 'hit': 1643, 'acc': 0.9158305462653289}})
errors:defaultdict(<function Classifier.evaluate.<locals>.<lambda> at 0x12211660>, {'neg': defaultdict(<class 'int'>, {'neg': 213, 'pos': 343}), 'pos': defaultdict(<class 'int'>, {'neg': 151, 'pos': 1643})})

<class 'yatk.ml.svm.SVM'> bogram bin 76.48226950354609: 76.5% __linear koef1 =0 koef2 =0 koef3 =0


>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
 ------------------------------------------------------
mic:0.7697872340425532
mac:0.6633965973322185
results:defaultdict(<function Classifier.evaluate.<locals>.<lambda> at 0x0B5CA228>, {'neg': {'miss': 298, 'hit': 256, 'acc': 0.4620938628158845}, 'pos': {'miss': 243, 'hit': 1553, 'acc': 0.8646993318485523}})
errors:defaultdict(<function Classifier.evaluate.<locals>.<lambda> at 0x1BE0F588>, {'neg': defaultdict(<class 'int'>, {'neg': 256, 'pos': 298}), 'pos': defaultdict(<class 'int'>, {'neg': 243, 'pos': 1553})})

 ------------------------------------------------------
mic:0.7025531914893617
mac:0.6105087633707849
results:defaultdict(<function Classifier.evaluate.<locals>.<lambda> at 0x0B6C9228>, {'neg': {'miss': 382, 'hit': 262, 'acc': 0.40683229813664595}, 'pos': {'miss': 317, 'hit': 1389, 'acc': 0.8141852286049238}})
errors:defaultdict(<function Classifier.evaluate.<locals>.<lambda> at 0x0EA42F18>, {'neg': defaultdict(<class 'int'>, {'neg': 262, 'pos': 382}), 'pos': defaultdict(<class 'int'>, {'pos': 1389, 'neg': 317})})

 ------------------------------------------------------
mic:0.7268085106382979
mac:0.650088713865328
results:defaultdict(<function Classifier.evaluate.<locals>.<lambda> at 0x153D0E40>, {'pos': {'miss': 300, 'hit': 1396, 'acc': 0.8231132075471698}, 'neg': {'miss': 342, 'hit': 312, 'acc': 0.47706422018348627}})
errors:defaultdict(<function Classifier.evaluate.<locals>.<lambda> at 0x0B6C90C0>, {'pos': defaultdict(<class 'int'>, {'neg': 300, 'pos': 1396}), 'neg': defaultdict(<class 'int'>, {'neg': 312, 'pos': 342})})

 ------------------------------------------------------
mic:0.7476595744680851
mac:0.6415881228142024
results:defaultdict(<function Classifier.evaluate.<locals>.<lambda> at 0x22915A08>, {'neg': {'miss': 324, 'hit': 249, 'acc': 0.43455497382198954}, 'pos': {'miss': 269, 'hit': 1508, 'acc': 0.8486212718064153}})
errors:defaultdict(<function Classifier.evaluate.<locals>.<lambda> at 0x110EF618>, {'neg': defaultdict(<class 'int'>, {'pos': 324, 'neg': 249}), 'pos': defaultdict(<class 'int'>, {'neg': 269, 'pos': 1508})})

 ------------------------------------------------------
mic:0.7493617021276596
mac:0.6351190476190476
results:defaultdict(<function Classifier.evaluate.<locals>.<lambda> at 0x1526E198>, {'pos': {'miss': 230, 'hit': 1520, 'acc': 0.8685714285714285}, 'neg': {'miss': 359, 'hit': 241, 'acc': 0.40166666666666667}})
errors:defaultdict(<function Classifier.evaluate.<locals>.<lambda> at 0x13728300>, {'pos': defaultdict(<class 'int'>, {'neg': 230, 'pos': 1520}), 'neg': defaultdict(<class 'int'>, {'neg': 241, 'pos': 359})})

 ------------------------------------------------------
mic:0.7714893617021277
mac:0.6492685450301966
results:defaultdict(<function Classifier.evaluate.<locals>.<lambda> at 0x1AB4E198>, {'neg': {'miss': 324, 'hit': 232, 'acc': 0.4172661870503597}, 'pos': {'miss': 213, 'hit': 1581, 'acc': 0.8812709030100334}})
errors:defaultdict(<function Classifier.evaluate.<locals>.<lambda> at 0x12C84ED0>, {'neg': defaultdict(<class 'int'>, {'neg': 232, 'pos': 324}), 'pos': defaultdict(<class 'int'>, {'neg': 213, 'pos': 1581})})

<class 'yatk.ml.svm.SVM'> unigram delta 74.46099290780141: 74.5% __linear koef1 =0 koef2 =0 koef3 =0


>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
 ------------------------------------------------------
mic:0.7327659574468085
mac:0.6085937060294437
results:defaultdict(<function Classifier.evaluate.<locals>.<lambda> at 0x219CA5D0>, {'neg': {'miss': 347, 'hit': 207, 'acc': 0.37364620938628157}, 'pos': {'miss': 281, 'hit': 1515, 'acc': 0.8435412026726058}})
errors:defaultdict(<function Classifier.evaluate.<locals>.<lambda> at 0x22AF3930>, {'neg': defaultdict(<class 'int'>, {'neg': 207, 'pos': 347}), 'pos': defaultdict(<class 'int'>, {'neg': 281, 'pos': 1515})})

 ------------------------------------------------------
mic:0.6885106382978723
mac:0.5708715312415806
results:defaultdict(<function Classifier.evaluate.<locals>.<lambda> at 0x16928930>, {'neg': {'miss': 444, 'hit': 200, 'acc': 0.3105590062111801}, 'pos': {'miss': 288, 'hit': 1418, 'acc': 0.8311840562719812}})
errors:defaultdict(<function Classifier.evaluate.<locals>.<lambda> at 0x220F4F60>, {'neg': defaultdict(<class 'int'>, {'neg': 200, 'pos': 444}), 'pos': defaultdict(<class 'int'>, {'neg': 288, 'pos': 1418})})

 ------------------------------------------------------
mic:0.688936170212766
mac:0.584394473775316
results:defaultdict(<function Classifier.evaluate.<locals>.<lambda> at 0x128614F8>, {'pos': {'miss': 305, 'hit': 1391, 'acc': 0.8201650943396226}, 'neg': {'miss': 426, 'hit': 228, 'acc': 0.3486238532110092}})
errors:defaultdict(<function Classifier.evaluate.<locals>.<lambda> at 0x12861C00>, {'pos': defaultdict(<class 'int'>, {'neg': 305, 'pos': 1391}), 'neg': defaultdict(<class 'int'>, {'neg': 228, 'pos': 426})})

 ------------------------------------------------------
mic:0.7093617021276596
mac:0.5855207268363155
results:defaultdict(<function Classifier.evaluate.<locals>.<lambda> at 0x22FF1E88>, {'neg': {'miss': 376, 'hit': 197, 'acc': 0.343804537521815}, 'pos': {'miss': 307, 'hit': 1470, 'acc': 0.827236916150816}})
errors:defaultdict(<function Classifier.evaluate.<locals>.<lambda> at 0x13AD7E88>, {'neg': defaultdict(<class 'int'>, {'pos': 376, 'neg': 197}), 'pos': defaultdict(<class 'int'>, {'neg': 307, 'pos': 1470})})

 ------------------------------------------------------
mic:0.7319148936170212
mac:0.6119047619047618
results:defaultdict(<function Classifier.evaluate.<locals>.<lambda> at 0x130D5078>, {'pos': {'miss': 250, 'hit': 1500, 'acc': 0.8571428571428571}, 'neg': {'miss': 380, 'hit': 220, 'acc': 0.36666666666666664}})
errors:defaultdict(<function Classifier.evaluate.<locals>.<lambda> at 0x17FA5420>, {'pos': defaultdict(<class 'int'>, {'neg': 250, 'pos': 1500}), 'neg': defaultdict(<class 'int'>, {'neg': 220, 'pos': 380})})

 ------------------------------------------------------
mic:0.734468085106383
mac:0.5822014629099396
results:defaultdict(<function Classifier.evaluate.<locals>.<lambda> at 0x254702B8>, {'neg': {'miss': 393, 'hit': 163, 'acc': 0.2931654676258993}, 'pos': {'miss': 231, 'hit': 1563, 'acc': 0.8712374581939799}})
errors:defaultdict(<function Classifier.evaluate.<locals>.<lambda> at 0x25392738>, {'neg': defaultdict(<class 'int'>, {'neg': 163, 'pos': 393}), 'pos': defaultdict(<class 'int'>, {'neg': 231, 'pos': 1563})})

<class 'yatk.ml.svm.SVM'> bigram delta 71.43262411347519: 71.4% __linear koef1 =0 koef2 =0 koef3 =0


>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
 ------------------------------------------------------
mic:0.7757446808510639
mac:0.6697906700007237
results:defaultdict(<function Classifier.evaluate.<locals>.<lambda> at 0x27FF82B8>, {'neg': {'miss': 294, 'hit': 260, 'acc': 0.4693140794223827}, 'pos': {'miss': 233, 'hit': 1563, 'acc': 0.8702672605790646}})
errors:defaultdict(<function Classifier.evaluate.<locals>.<lambda> at 0x28708618>, {'neg': defaultdict(<class 'int'>, {'neg': 260, 'pos': 294}), 'pos': defaultdict(<class 'int'>, {'neg': 233, 'pos': 1563})})

 ------------------------------------------------------
mic:0.6978723404255319
mac:0.6097014191782019
results:defaultdict(<function Classifier.evaluate.<locals>.<lambda> at 0x10314C48>, {'neg': {'miss': 377, 'hit': 267, 'acc': 0.41459627329192544}, 'pos': {'miss': 333, 'hit': 1373, 'acc': 0.8048065650644783}})
errors:defaultdict(<function Classifier.evaluate.<locals>.<lambda> at 0x10314B28>, {'neg': defaultdict(<class 'int'>, {'neg': 267, 'pos': 377}), 'pos': defaultdict(<class 'int'>, {'neg': 333, 'pos': 1373})})

 ------------------------------------------------------
mic:0.7293617021276596
mac:0.6476301497316946
results:defaultdict(<function Classifier.evaluate.<locals>.<lambda> at 0x250E4D68>, {'pos': {'miss': 285, 'hit': 1411, 'acc': 0.8319575471698113}, 'neg': {'miss': 351, 'hit': 303, 'acc': 0.463302752293578}})
errors:defaultdict(<function Classifier.evaluate.<locals>.<lambda> at 0x2505F9C0>, {'pos': defaultdict(<class 'int'>, {'neg': 285, 'pos': 1411}), 'neg': defaultdict(<class 'int'>, {'neg': 303, 'pos': 351})})

 ------------------------------------------------------
mic:0.7570212765957447
mac:0.6584204214998512
results:defaultdict(<function Classifier.evaluate.<locals>.<lambda> at 0x225EDA98>, {'neg': {'miss': 306, 'hit': 267, 'acc': 0.46596858638743455}, 'pos': {'miss': 265, 'hit': 1512, 'acc': 0.8508722566122678}})
errors:defaultdict(<function Classifier.evaluate.<locals>.<lambda> at 0x281C8D68>, {'neg': defaultdict(<class 'int'>, {'pos': 306, 'neg': 267}), 'pos': defaultdict(<class 'int'>, {'neg': 265, 'pos': 1512})})

 ------------------------------------------------------
mic:0.7672340425531915
mac:0.6575238095238095
results:defaultdict(<function Classifier.evaluate.<locals>.<lambda> at 0x284BA738>, {'pos': {'miss': 207, 'hit': 1543, 'acc': 0.8817142857142857}, 'neg': {'miss': 340, 'hit': 260, 'acc': 0.43333333333333335}})
errors:defaultdict(<function Classifier.evaluate.<locals>.<lambda> at 0x1A729588>, {'pos': defaultdict(<class 'int'>, {'neg': 207, 'pos': 1543}), 'neg': defaultdict(<class 'int'>, {'neg': 260, 'pos': 340})})

 ------------------------------------------------------
mic:0.7714893617021277
mac:0.6474068237049156
results:defaultdict(<function Classifier.evaluate.<locals>.<lambda> at 0x1310A4F8>, {'neg': {'miss': 327, 'hit': 229, 'acc': 0.4118705035971223}, 'pos': {'miss': 210, 'hit': 1584, 'acc': 0.882943143812709}})
errors:defaultdict(<function Classifier.evaluate.<locals>.<lambda> at 0x0ABADF18>, {'neg': defaultdict(<class 'int'>, {'neg': 229, 'pos': 327}), 'pos': defaultdict(<class 'int'>, {'neg': 210, 'pos': 1584})})

<class 'yatk.ml.svm.SVM'> bogram delta 74.97872340425532: 75.0% __linear koef1 =0 koef2 =0 koef3 =0


