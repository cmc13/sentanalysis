

import sys
#sys.path.append('C:/Users/asus/PycharmProjects/sentClassyfier/python')

import svmutil as svm
import svm as svm1
import src.sentiment.ml as ml
import pickle

class SVM2(ml.Classifier):
    def __init__(self):
        self._labels = ml.Autoincrement()
        self._features = ml.Autoincrement()
        self._regression = False

    def __repr__(self):
        return 'SVM'

    def save(self, path):
        svm.svm_save_model(path + '-model', self._model)
        del(self._model)
        f = open(path, 'wb')
        pickle.dump((self._labels, self._features), f)
        f.close()

    @staticmethod
    def load(path):
        obj = SVM2()
        f = open(path, 'rb')
        obj._labels, obj._features = pickle.load(f)
        f.close()
        obj._model = svm.svm_load_model(path + '-model')
        return obj

    def getKernelType(self,kerneltype):
        if kerneltype == 'linear':
            return 0;
        if kerneltype == 'polynomial':
            return 1;
        if kerneltype == 'radial_basis':
            return 2;
        if kerneltype == 'sigmoid':
            return 3;

    def setAllId(self,words):
        for word in words:
            self._features.setId(word)


    def train(self, x, y, kerneltype,koef1,koef2,koef3, biased = False, ):
        data = []
        for sample in x:
            data.append(dict([(self._features.setId(d), sample[d]) for d in sample]))

        #for item in data.items():

        labels = [self._labels.setId(C) for C in y]
        if self._labels.count() == 2:
            labels = [1 if label == 1 else -1 for label in labels]
            #param = svm1.svm_parameter('-c 0.01 -s 0 -t 0');# -d 2 -r 1 -g 0.0000001 ');#'+str(self.getKernelType(kerneltype))+' -q' )# (' -B {0}'.format(biased) if biased else ''))
            #param = svm1.svm_parameter('-c 0.01 -s 0 -t 1 -d 2 -r 0 -g 0.01 ');
            #param = svm1.svm_parameter('-c 0.01 -s 0 -t 2 -g 0.1 -h 0 -q'); nothing
            #param = svm1.svm_parameter('-c 0.01 -s 0 -t 3 -g 1 -r 0 ');
            kernelNumber = self.getKernelType(kerneltype)
            if kernelNumber == 0 :
                param = svm1.svm_parameter('-c 0.01 -s 0 -t 0 -q');
            else:
                if kernelNumber == 1 :
                    stroke = '-c 0.01 -s 0 -t 1 -d '+ str(koef1) + ' -r ' + str(koef2) + ' -g ' + str(koef3)+ ' -q'
                    print('>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>')
                    print(stroke)
                    param = svm1.svm_parameter(stroke);
                else :
                    if kernelNumber == 3 :
                        stroke = '-c 0.01 -s 0 -t 3 -g '+ str(koef1) + ' -r ' + str(koef2) + ' -q'
                        print('>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>')
                        print(stroke)
                        param = svm1.svm_parameter(stroke);



        else:
            #param = svm1.svm_parameter('-c 0.01 -s 0 -t 0');# -d 2 -r 1 -g 0.0000001 '); #'+str(self.getKernelType(kerneltype))+' -q' )#+ (' -B {0}'.format(biased) if biased else ''))
            #param = svm1.svm_parameter('-c 0.01 -s 0 -t 1 -d 2 -r 0 -g 0.01 ');
            #param = svm1.svm_parameter('-c 0.01 -s 0 -t 2 -g 0.1 -h 0 -q');
            pass
        prob = svm.svm_problem(labels, data)
        self._model = svm.svm_train(prob, param)
        '''
            -s svm_type : set type of SVM (default 0)
	        0 -- C-SVC		(multi-class classification)
	        1 -- nu-SVC		(multi-class classification)
	        2 -- one-class SVM
	        3 -- epsilon-SVR	(regression)
	        4 -- nu-SVR		(regression)
	    -t kernel_type : set type of kernel function (default 2)
	        0 -- linear: u'*v
	        1 -- polynomial: (gamma*u'*v + coef0)^degree
	        2 -- radial basis function: exp(-gamma*|u-v|^2)
	        3 -- sigmoid: tanh(gamma*u'*v + coef0)
	        4 -- precomputed kernel (kernel values in training_set_file)
	    -d degree : set degree in kernel function (default 3)
	    -g gamma : set gamma in kernel function (default 1/num_features)
	    -r coef0 : set coef0 in kernel function (default 0)
	    -c cost : set the parameter C of C-SVC, epsilon-SVR, and nu-SVR (default 1)
	    -n nu : set the parameter nu of nu-SVC, one-class SVM, and nu-SVR (default 0.5)
	    -p epsilon : set the epsilon in loss function of epsilon-SVR (default 0.1)
	    -m cachesize : set cache memory size in MB (default 100)
	    -e epsilon : set tolerance of termination criterion (default 0.001)
	    -h shrinking : whether to use the shrinking heuristics, 0 or 1 (default 1)
	    -b probability_estimates : whether to train a SVC or SVR model for probability estimates, 0 or 1 (default 0)
	    -wi weight : set the parameter C of class i to weight*C, for C-SVC (default 1)
	    -v n: n-fold cross validation mode
	    -q : quiet mode (no outputs)
	'''


    def train_regression(self, x, y):
        data = []
        for sample in x:
            data.append(dict([(self._features.setId(d), sample[d]) for d in sample]))

        self._regression = True
        param = svm.svm_parameter('-c 1 -s 0 -q')
        prob = svm.svm_problem(y, data)
        self._model = svm.svm_train(prob, param)

    def predict(self, x):

        y = []
        for sample in x:
            data = dict([(self._features.getId(d), sample[d]) for d in sample if self._features.getId(d)])
            label, _, _ = svm.svm_predict([0], [data], self._model, '')
            if self._regression:
                y.append(label[0])
            else:
                if self._labels.count() == 2:
                    label[0] = 1 if label[0] == 1 else 2
                y.append(self._labels.getVal(label[0]))

        return y